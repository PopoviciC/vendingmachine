package com.sda.vendingmachine.service;

import com.sda.vendingmachine.persistence.entities.Item;
import com.sda.vendingmachine.service.exceptions.NotFullPaidException;
import com.sda.vendingmachine.service.exceptions.NotSufficientChangeException;

public class VendingStorageImpl implements IVendingStorage {


    @Override
    public boolean checkInsertedAmount(ItemType itemType) throws NotFullPaidException {
        return false;
    }

    @Override
    public boolean checkForChange() throws NotSufficientChangeException {
        return false;
    }

    @Override
    public Item releaseItem(Item selectedItem) {
        return null;
    }

    @Override
    public void addMoney(int value) {

    }

    //TODO: implement all storage logic here

}
